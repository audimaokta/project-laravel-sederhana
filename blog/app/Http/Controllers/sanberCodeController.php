<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class sanberCodeController extends Controller
{
    public function homepage()
    {
        return view('halaman.homepage');
    }

    public function regist()
    {
        return view('halaman.register');
    }
    
    public function submit(Request $request)
    {
        
       $nama = $request["nama"];
       return view('halaman.submit', compact('nama'));
}
}